package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.EquipoDeFutbol;
import model.Jugador;

public class Main {
	private static Scanner entradaEscaner;

	public static void main(String[] args) {
	     entradaEscaner = new Scanner(System.in);
	     EquipoDeFutbol equipo = new EquipoDeFutbol();
	     
	     System.out.println("Digite el nombre del equipo:");
	     String nombre = entradaEscaner.nextLine();
	     equipo.setNombre(nombre); 
	     System.out.println("Digite el estadio del equipo:");
	     String estadio = entradaEscaner.nextLine();
	     equipo.setEstadio(estadio);
	     System.out.println("Digite la cantidad de titulos:");
	     int titulos = entradaEscaner.nextInt();
	     equipo.setTitulos(titulos);
	     
	     System.out.println("Digite el listado de jugadores titulares");
	     System.out.println("----------------------------------------");
	     boolean listadoTitulares = true;
	     List<Jugador> jugadoresTitulares = new ArrayList<Jugador>();
	     while(listadoTitulares) {
	    	 entradaEscaner = new Scanner(System.in);
	    	 Jugador jugador = new Jugador();
		     System.out.println("Digite el nombre del jugador titular:");
		     String nombreJugador = entradaEscaner.nextLine();
		     jugador.setNombre(nombreJugador);
		     
		     System.out.println("Digite la posici�n del jugador titular:");
		     String posicion = entradaEscaner.nextLine();
		     jugador.setPosicion(posicion);
		     		     
		     jugadoresTitulares.add(jugador);
		     
		     System.out.println("Desea agregar otro jugador a la lista de titulares?"
		     		+ " digita si o no");
		     String agregar = entradaEscaner.nextLine();
		     if(agregar.equalsIgnoreCase("no")) {
		    	 listadoTitulares = false;
		     }
	     }
	     equipo.setJugadoresTitulares(jugadoresTitulares);
	     
	     System.out.println("Digite el listado de jugadores suplentes");
	     System.out.println("----------------------------------------");
	     boolean listadoSuplentes = true;
	     List<Jugador> jugadoresSuplentes = new ArrayList<Jugador>();
	     while(listadoSuplentes) {
	    	 entradaEscaner = new Scanner(System.in);
	    	 Jugador jugador = new Jugador();
		     System.out.println("Digite el nombre del jugador suplente:");
		     String nombreJugador = entradaEscaner.nextLine();
		     jugador.setNombre(nombreJugador);
		     
		     System.out.println("Digite la posici�n del jugador suplente:");
		     String posicion = entradaEscaner.nextLine();
		     jugador.setPosicion(posicion);
		     		     
		     jugadoresSuplentes.add(jugador);
		     
		     System.out.println("Desea agregar otro jugador a la lista de suplentes?"
		     		+ " digita si o no");
		     String agregar = entradaEscaner.nextLine();
		     if(agregar.equalsIgnoreCase("no")) {
		    	 listadoSuplentes = false;
		     }
	     }
	     equipo.setJugadoresSuplentes(jugadoresSuplentes);
	     
	     System.out.println("Si desea generar el Reporte b�sico "
	     	+ "(Nombre del equipo, t�tulos ganados y cantidad total de jugadores)"
	     	+ " digita el numero 1 \n o el numero 2 si desea generar el reporte detallado"
	     	+ " (Nombre del equipo, listado con el detalle de todos los titulares"
	     	+ " y todos los suplentes)");
	     int reporte = entradaEscaner.nextInt();
	     
	     if(reporte == 1) {
	    	 System.out.println("");
	    	 System.out.println("----------------------------------------------");
	    	 System.out.println("Nombre del equipo: "+ equipo.getNombre());
	    	 System.out.println("T�tulos ganados: "+ equipo.getTitulos());
	    	 System.out.println("Cantidad total de jugadores: "+ (equipo.getJugadoresTitulares().size()
	    			 + equipo.getJugadoresSuplentes().size()));
	     }else if(reporte == 2) {
	    	 System.out.println("");
	    	 System.out.println("----------------------------------------------");
	    	 System.out.println("Nombre del equipo: "+ equipo.getNombre());
    		 System.out.println("---------- JUGADORES TITULARES ----------");
    		 System.out.println("      Nombres       |      posici�n    ");
    		 
	    	 for(Jugador jugador : equipo.getJugadoresTitulares()) {
	    		 System.out.println(jugador.getNombre()+"   --   "+ jugador.getPosicion());
	    	 }
	    	 System.out.println("");
	    	 System.out.println("---------- JUGADORES SUPLENTES ----------");
    		 System.out.println("      Nombres       |      posici�n    ");

	    	 for(Jugador jugador : equipo.getJugadoresSuplentes()) {
	    		 System.out.println(jugador.getNombre()+"  --   "+ jugador.getPosicion());
	    	 }
	     }
	     
	}
}
