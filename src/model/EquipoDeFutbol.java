package model;

import java.util.List;

public class EquipoDeFutbol {
	
	private String nombre;
	private String estadio;
	private int titulos;
	private List<Jugador> jugadoresTitulares;
	private List<Jugador> jugadoresSuplentes;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEstadio() {
		return estadio;
	}
	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}
	public int getTitulos() {
		return titulos;
	}
	public void setTitulos(int titulos) {
		this.titulos = titulos;
	}
	public List<Jugador> getJugadoresTitulares() {
		return jugadoresTitulares;
	}
	public void setJugadoresTitulares(List<Jugador> jugadoresTitulares) {
		this.jugadoresTitulares = jugadoresTitulares;
	}
	public List<Jugador> getJugadoresSuplentes() {
		return jugadoresSuplentes;
	}
	public void setJugadoresSuplentes(List<Jugador> jugadoresSuplentes) {
		this.jugadoresSuplentes = jugadoresSuplentes;
	}
	
	@Override
	public String toString() {
		return "EquipoDeFutbol [nombre=" + nombre + ", estadio=" + estadio + ", titulos=" + titulos
				+ ", jugadoresTitulares=" + jugadoresTitulares + ", jugadoresSuplentes=" + jugadoresSuplentes + "]";
	}
	
	
}
